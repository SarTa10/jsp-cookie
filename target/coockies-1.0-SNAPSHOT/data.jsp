<%--
  Created by IntelliJ IDEA.
  User: SarTa
  Date: 08.11.2021
  Time: 20:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

    Cookie username =  new Cookie("username", request.getParameter("username"));
    Cookie email =  new Cookie("email", request.getParameter("email"));

    username.setMaxAge(60 * 60 * 24);
    email.setMaxAge(60 * 60 * 24);

    response.addCookie(username);
    response.addCookie(email);
%>
<html>
<head>


</head>
<body>
    <ul>
        <li>
            <p> username: </p>
            <%= request.getParameter("username")%>
        </li>
        <li>
            <p> email: </p>
            <%= request.getParameter("email")%>
        </li>

    </ul>

    <%

        Cookie cookie = null;
        Cookie[] cookies = null;

        cookies = request.getCookies();


        if( cookies != null ) {
            out.println("<h2> Found Cookies Name and Value</h2>");

            for (int i = 0; i < cookies.length; i++) {
                cookie = cookies[i];
                out.print("Name : " + cookie.getName( ) + ",  ");
                out.print("Value: " + cookie.getValue( )+" <br/>");
            }
        } else {
            out.println("<h2>No cookies founds</h2>");
        }

    %>
</body>
</html>
