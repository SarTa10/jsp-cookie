<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<h1><%= "Hello World!" %>
</h1>
<br/>
<form action="data.jsp" method="GET" target="_blank">
    username: <input type="text" name="username">
    <br>
    email: <input type="text" name="email">
    <br>
    <input type="submit" value="Submit">
</form>
</body>
</html>